using System;
using System.Collections.Generic;

namespace CodeWars
{
    public class Kata
    {
        private const int NotFound = -1;

        private const int UniqueDigitsCount = 10;

        public static long NextSmaller(long n)
        {
            var digits = NumberToDigits(n);
            var visitedDigits = new int[UniqueDigitsCount];

            for (var i = digits.Length - 2; i >= 0; i--)
            {
                var previous = digits[i+1];
                var current = digits[i];

                visitedDigits[previous]++;
                
                var nextLesserDigit = FindNextLesserDigit(current, i, visitedDigits);

                if (nextLesserDigit == NotFound)
                {
                    continue;
                }

                return RearrangeDigits(digits, visitedDigits, i, nextLesserDigit);
            }

            return NotFound;
        }

        private static int[] NumberToDigits(long n)
        {
            const int @base = 10;

            var rest = n;
            var digits = new List<int>();

            while (rest > 0)
            {
                var remainder = (int)(rest % @base);

                digits.Add(remainder);
                rest /= @base;
            }

            var result = digits.ToArray();
            
            Array.Reverse(result);

            return result;
        }

        private static int FindNextLesserDigit(int n, int currentIndex, int[] numbers)
        {
            for (var i = n - 1; i >= 0; i--)
            {
                if (i == 0 && currentIndex == 0)
                {
                    continue;
                }

                if (numbers[i] > 0)
                {
                    return i;
                }
            }

            return NotFound;
        }

        private static long RearrangeDigits(IList<int> digits, IList<int> visitedDigits, int index, int nextLesserDigit)
        {
            var current = digits[index];
            
            visitedDigits[current]++;
            visitedDigits[nextLesserDigit]--;

            digits[index] = nextLesserDigit;

            index++;

            for (var j = visitedDigits.Count - 1; j >= 0; j--)
            {
                var digitsCount = visitedDigits[j];
                if (digitsCount == 0)
                {
                    continue;
                }

                while (digitsCount > 0)
                {
                    if (index >= digits.Count)
                    {
                        return NotFound;
                    }

                    digits[index] = j;

                    digitsCount--;
                    index++;
                }
            }

            var numberAsString = string.Join(string.Empty, digits);

            return long.Parse(numberAsString);
        }
    }
}
