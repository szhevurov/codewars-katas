
# Next smaller number with the same digits
Write a function that takes a positive integer and returns the next smaller positive integer containing the same digits.

For example:

```csharp
nextSmaller(21) == 12
nextSmaller(531) == 513
nextSmaller(2071) == 2017
```

Return -1 (for  `Haskell`: return  `Nothing`), when there is no smaller number that contains the same digits. Also return -1 when the next smaller number with the same digits would require the leading digit to be zero.

```csharp
nextSmaller(9) == -1
nextSmaller(111) == -1
nextSmaller(135) == -1
nextSmaller(1027) == -1 // 0721 is out since we don't write numbers with leading zeros
```
-   some tests will include very large numbers.
-   test data only employs positive integers.

# Solution Approach

 1. We need to split the number into digits:
```csharp
const int @base = 10;
var rest = n;
var digits = new List<int>();

while (rest > 0)
{
   var remainder = (int)(rest % @base);
   digits.Add(remainder);
   rest /= @base;
}
```
 2. Reverse the array 
```csharp
digits.Reverse();
```
 3. Visit every digit starting from the last one:
	 - Continue If there are no preceding digits lesser than the current one
	 - Otherwise, replace the current digit with the lesser one
	 - Add the rest of the visited digits (including the digit we replaced in the prev. step) in desc order to the end of the array.

```csharp
for (var i = digits.Length - 2; i >= 0; i--)
{
   var previous = digits[i+1];
   var current = digits[i];

   visitedDigits[previous]++;

   var nextLesserDigit = FindNextLesserDigit(current, i, visitedDigits);

   if (nextLesserDigit == NotFound)
   {
       continue;
   }

   return RearrangeDigits(digits, visitedDigits, i, nextLesserDigit);
}
```