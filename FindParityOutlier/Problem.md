# Find the Parity Outlier
You are given an array (which will have a length of at least 3, but could be very large) containing integers.
The array is either entirely comprised of odd integers or entirely comprised of even integers except for a single integer `N`. 
Write a method that takes the array as an argument and returns this "outlier" `N`

## Examples
```csharp
[2, 4, 0, 100, 4, 11, 2602, 36]
Should return: 11 (the only odd number)

[160, 3, 1719, 19, 11, 13, -21]
Should return: 160 (the only even number)
```
## Solution Approach
We need to take the first 3 numbers, divide each one by 2 and sum the remainders. If the sum is less or equal than 1 then we need to find an odd number, otherwise - an even one : 
```csharp
var remainder  = 0;
for(var i = 0; i < 3; i++) {
   remainder += integers[i] % 2;
}

if(remainder <= 1) {
   return n => n % 2 > 0;
}

return n => n % 2 == 0;
```