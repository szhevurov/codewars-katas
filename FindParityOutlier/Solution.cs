using System;
using System.Linq;

namespace CodeWars
{
    public class Kata
    {
        public static int Find(int[] integers)
        {
            var predicate = GetFilterPredicate(integers);

            return integers.First(predicate);
        }
  
        public static Func<int, bool> GetFilterPredicate(int[] integers)
        {
            const int maxRemainderSumForOddPredicate = 1;

            var first = integers[0];
            var second = integers[1];
            var third = integers[2];

            var remainderSum = first % 2 + second % 2 + third % 2;

            // If the sum of the remainders is greater than 1 then we have at least two odd inters and need to find an even one.
            if (remainderSum <= maxRemainderSumForOddPredicate)
            {
                return number => number % 2 > 0;
            }

            return number => number % 2 == 0;
        }
    }
}
