using System;

namespace Solution
{
  public static class Kata
  {
    public static int ShortestStepsToNum(int num)
    {
      var steps = 0;
      
      if(num == 1) {
        return steps;
      }
      
      while(num > 1) {
        var remainder = num % 2;
        
        if(remainder > 0) {
          num -= 1;
          
          steps++;
        }
        
        
        num /= 2;
        steps++;
      }
      
      return steps;
    }
  }
}