const chalk = require('chalk');

function square_sums_row_old(n) {
	let firstElementSqrt = Math.floor(Math.sqrt(n));
	let firstElement = Math.pow(firstElementSqrt, 2);
	
	let integersInUse = Array(n + 1).fill(false);
	let transformedArray = [Array(n)];

	integersInUse[firstElement] = true;
	transformedArray[0] = firstElement;

	reachableSquares = findReachableSquares(n);

	if(!reachableSquares.length) {
		return false;
	}

	for(let i = 1; i < n; i++ ) {
		let prevInteger = transformedArray[i - 1];
		let currentInteger = 0;

		for(let j = 0; j < reachableSquares.length; j++) {
			let square = reachableSquares[j];

			if(square < prevInteger) {
				continue;
			}

			let distanceToNearestSquare = square - prevInteger;

			// integer is already in use
			if(distanceToNearestSquare === 0 || distanceToNearestSquare > n || integersInUse[distanceToNearestSquare]) {
				continue;
			}

			currentInteger = distanceToNearestSquare;

			break;
		}

		if(!currentInteger) {
			console.log(transformedArray.join(", "));

			return false;
		}

		integersInUse[currentInteger] = true;
		transformedArray[i] = currentInteger;
	}

	console.log(transformedArray);

	return true;
}

function square_sums_row(n) {
	let firstElement = Math.floor(n * 0.6);
	
	let integersInUse = Array(n + 1).fill(false);
	let transformedArray = [Array(n)];

	integersInUse[firstElement] = true;
	transformedArray[0] = firstElement;

	reachableSquares = findReachableSquares(n);	

	if(!reachableSquares.length) {
		return false;
	}

	for(let i = firstElement; i < n; i++ ) {
		integersInUse[i] = true;
		transformedArray[0] = i;
		
		let result = addNextInteger(1, i, n, integersInUse, reachableSquares, transformedArray);
		
		if(result) {
			return true;
		}
		
		integersInUse[i] = false;
	}

	return false;
}

function addNextInteger(index, currentInt, n, integersInUse, reachableSquares, transformedArray) {
	for(let square of reachableSquares) {
		if(square <= currentInt) {
			continue;
		}

		let distanceToNearestSquare = square - currentInt;
		
		if(distanceToNearestSquare === 0 || distanceToNearestSquare > n || integersInUse[distanceToNearestSquare]) {
			continue;
		}
		
		let nextInt = distanceToNearestSquare;
		let nextIndex = index + 1;
			
		transformedArray.push(nextInt);
		integersInUse[nextInt] = true;
		
		console.log(`${transformedArray.join(", ")} - (${transformedArray.length})`);
		
		if(nextIndex === n) {
			return true;
		}
		
		let result = addNextInteger(nextIndex, nextInt, n, integersInUse, reachableSquares, transformedArray);
		
		if(result) {
			return true;
		}
		
		integersInUse[nextInt] = false;
		transformedArray.pop();
	}
	
	return false;
}

function findReachableSquares(n) {
	// 1 and 2 shouldn't be taken into account otherwise we won't be able to build a perfect square array
	const minSquareBase = 3;

	let maxSquareBase = Math.floor(Math.sqrt(n + (n - 1)));
	let currentSquareBase = minSquareBase;

	let reachableSquares = [];

	while(currentSquareBase <= maxSquareBase) {
		reachableSquares.push(Math.pow(currentSquareBase, 2));

		currentSquareBase++;
	}

	return reachableSquares;
}

const n = 43;

let result = square_sums_row(n);

console.log(`An array of ${n} integers is transformable? : ${result}`);