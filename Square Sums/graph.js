const chalk = require('chalk');

class SquareSumSolver {
	constructor(){}
	
	solve(n) {
		let reachableSquares = this.findReachableSquares(n);
		let graph = new Graph();

		for(let i = 1; i <= n; i++) {
            let perfectSquareDistances = reachableSquares.map(s => s - i).filter(s => s > 0 && s !== i && s <= n);

            graph.add(i, perfectSquareDistances);
        }

        //graph.print(console.log);
        return graph.hasHamiltonianPath();
	}
	
	findReachableSquares(n) {
		// 1 and 2 shouldn't be taken into account otherwise we won't be able to build a perfect square array
		const minSquareBase = 2;

		let maxSquareBase = Math.floor(Math.sqrt(n + (n - 1)));
		let currentSquareBase = minSquareBase;

		let reachableSquares = [];

		while(currentSquareBase <= maxSquareBase) {
			reachableSquares.push(Math.pow(currentSquareBase, 2));

			currentSquareBase++;
		}

		return reachableSquares;
	}

	isPerfectSquareArray(array, n) {
	    if(array.length !== n) {
	        return false;
        }

        let reachableSquares = this.findReachableSquares(n);
        let visitedNumbers = new Set();

	    for(let i = 1; i < array.length; i++) {
	        let prev = array[i - 1];
	        let current = array[i];

	        if(visitedNumbers.has(current) || current > n || current <= 0)
            {
                return false;
            }

            let sum = current + prev;

	        if(!reachableSquares.some(s => s === sum)) {
	            return false;
            }

            visitedNumbers.add(current);
        }

        return true;
    }
}

class Vertex {
	constructor(value){
		if(!value) {
			throw new Error('Invalid value');
		}
		
		this.value = value;
		this.adjacentVertices = new Set();
	}
	
	get hasEdges() {
		return this.adjacentVertices.size > 0;
	}
	
	get degree() {
		return this.adjacentVertices.size
	}
	
	add(node) {
		if(!node || this.adjacentVertices.has(node)) {
			return;
		}
		
		this.adjacentVertices.add(node);
	}
}

class Graph {
	constructor() {
		this.vertices = new Map();
	}
	
	get isConnected() {
		return [...this.vertices.values()].every(n => n.hasEdges);
	}

	get vertexCount() {
	    return this.vertices.size;
    }
	
	add(number, adjacentNumbers) {
		if(!number || number < 0) {
			return;
		}
		
		let vertex = this.vertices.get(number);

		if(!vertex) {
            vertex = new Vertex(number);
        }
		
		this.vertices.set(vertex.value, vertex);
		
		if(!adjacentNumbers) {
			return;
		}
		
		for(let num of adjacentNumbers) {
			let adjacentVertex = this.vertices.get(num);
			
			if(!adjacentVertex) {
				adjacentVertex = new Vertex(num);
				
				this.vertices.set(num, adjacentVertex);
			}

			vertex.add(adjacentVertex);
			adjacentVertex.add(vertex);
		}
	}
	
	hasHamiltonianPath() {
	    let result = {success: false, path: [] };

		if(!this.vertices.size || !this.isConnected) {
			return result;
		}

        let sortedVertices = [...this.vertices.values()].sort((a, b) => a.degree - b.degree);
        let minEdgesCount = sortedVertices[0].degree;

        for(let vertex of sortedVertices.filter(v => v.degree === minEdgesCount)) {
            let visited = new Set();
            let path = [];

            Graph._findHamiltonianPath(this, vertex, visited, path);

            if(path.length === sortedVertices.length) {
                result.success = true;
                result.path = path;

                break;
            }
        }

        return result;
    }

    print(printFn) {
	    for(let vertex of [...this.vertices.values()].sort((a, b) => a.value - b.value)) {
	        printFn(`Node ${vertex.value} \r\n ${[...vertex.adjacentVertices.values()].map(v => v.value).join(', ')} \r\n`);
        }
    }
	
	static _findHamiltonianPath(graph, currentVertex, visitedVertices, path, n) {
		visitedVertices.add(currentVertex);
        path.push(currentVertex);

        //console.log(path.map(v => v.value).join(', '));

        for(let vertex of currentVertex.adjacentVertices) {
            if(visitedVertices.has(vertex)) {
                continue;
            }

            Graph._findHamiltonianPath(graph, vertex, visitedVertices, path);
        }

        if(path.length !== graph.vertexCount) {
            path.pop();
            visitedVertices.delete(currentVertex);
        }
	}
}

const FROM = 10;
const TO = 43;

let solver = new SquareSumSolver();

let total = 0;

for(let i = FROM; i <= TO; i++) {
    let result = solver.solve(i);
    let questionMessage = chalk.green(`Is it possible to rearrange an array of ${i} integers?`);

    if(result.success) {
        total++;
        console.log(`${i}:\t${result.path.map(v => v.value).join(', ')}`);
        //console.log(`Is valid array? - ${chalk.red(solver.isPerfectSquareArray(result.path.map(v => v.value), i))}\r\n`);
    }
}

console.log(`Total perfect square arrays found in range ${FROM} - ${TO}: ${total}`);











